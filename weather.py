import requests
import config


def query_api(city):
    try:
        print(config.API_URL.format(city, config.API_KEY))
        data = requests.get(config.API_URL.format(city, config.API_KEY)).json()
    except Exception as exc:
        print(exc)
        data = None
    return data
