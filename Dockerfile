# our base image
FROM ubuntu:latest
MAINTAINER sigal "mariiasigal@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential


COPY . /weatherapp
ENV HOME =/weatherapp
WORKDIR weatherapp

RUN pip install -r requirements.txt

# specify the port number the container should expose
EXPOSE 5000

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:5000", "-w", "4", "app:app"]