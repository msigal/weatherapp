import os

API_URL = ('http://api.openweathermap.org/data/2.5/weather?q={}'
           '&mode=json&units=metric&appid={}')
API_KEY = os.environ['API_KEY']