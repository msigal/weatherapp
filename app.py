#!/usr/bin/env python
from pprint import pprint as pp
import flask
from weather import query_api

data = [
    {'name': 'Kazan'},
    {'name': 'Moscow'},
    {'name': 'Ulyanovsk'},
    {'name': 'Saransk'},
    {'name': 'Samara'},
    {'name': 'Saratov'},
    {'name': 'Dimitrovgrad'},
    {'name': 'Boston'},
    {'name': 'ErrorCode'}
]
app = flask.Flask(__name__)


@app.route('/')
def index():
    return flask.render_template(
        'weather.html',
        data=data
    )


def get_select():
    """
    returrn selected city from the list
    """
    return flask.request.form.get('comp_select')


@app.route("/result", methods=['GET', 'POST'])
def result():
    """
    process the result and send it to the template
    """
    data = []
    select = get_select()
    resp = query_api(select)
    # if response and response code ok
    if resp and resp['cod'] == 200:
        data.append(resp)
        return flask.render_template(
            'result.html',
            data=data
        )
    # any other case
    else:
        return flask.render_template(
            'errorcode.html',
            data=data
        )


if __name__ == '__main__':
    app.run(debug=True)
